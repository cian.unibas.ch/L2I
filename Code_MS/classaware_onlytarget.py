import torch
from skimage import exposure
import matplotlib as mpl
from pytorchtools import EarlyStopping
import os
import torch.nn.functional as nnf
os.environ['OMP_NUM_THREADS'] = '4'
os.environ['CUDA_VISIBLE_DEVICES'] ='0'
from crossvalidation import RetinaDataset
from torch.utils.data import Dataset,  DataLoader
torch.set_num_threads(4)
mpl.use('Agg')
import numpy as np
from sklearn.metrics import roc_auc_score
from visdom import Visdom
viz = Visdom(port=8850)
from mhatonumpy import get_patch, visualize_ch, visualize, classification_loss, kappa_score,  standardizetensor, apply_augmentation, standardize_ch
import torch.nn as nn
from torch.autograd import Variable

from inception_resnet import InceptionResnetV1
id =0
ACTIVATION = nn.ReLU()

torch.cuda.set_device(id)
device = 'cuda'
print("computations done on ", device)


class Solver(object):
    def __init__(self, path, ablation=None):
        super(Solver, self).__init__()
        self.num_latent = 128
        self.netD = InceptionResnetV1(flag='update_all').to(device)
        PATH = path

        p2 = np.array([np.array(p.shape).prod() for p in self.netD.parameters()]).sum()
        print('number of parameters', p2)

        blank = np.ones((256, 256))
        self.lossd_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                opts=dict(xlabel='epoch', ylabel='Loss', title='training loss discriminator'))

        self.image_window1 = viz.image(blank);self.image_window2 = viz.image(blank);self.image_window3 = viz.image(blank)
        self.image_window4 = viz.image(blank);self.image_window5 = viz.image(blank);self.image_window6 = viz.image(blank)
        self.image_window7 = viz.image(blank);self.image_window8 = viz.image(blank);self.image_window9 = viz.image(blank)

        self.grad_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                               opts=dict(xlabel='epoch', ylabel='gradient', title='average gradients'))
        self.val_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                              opts=dict(xlabel='epoch', ylabel='accuracy', title='classification accuracy on validation set'))


        self.latent_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                 opts=dict(xlabel='epoch', ylabel='Loss', title='latent 0'))
        self.latent_window2 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                 opts=dict(xlabel='epoch', ylabel='Loss', title='latent 1'))
        self.latent_window3 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                       opts=dict(xlabel='epoch', ylabel='Loss', title='latent c1'))
        self.latent_window4 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                       opts=dict(xlabel='epoch', ylabel='Loss', title='latent c2'))

        self.batchsize =6
        self.lambda_cls_d = 1
        self.lambda_latent=1
        self.lambda_org=100

        self.n_critic = 1
        self.lr_update_step = 1000
        self.num_iters = 200000
        self.num_iters_decay = 100000

        self.valset_target = RetinaDataset(filename_csv='./namestotal1.csv', index='validation_data_target.csv',
                                           image_path=PATH)
        self.Testset_target = RetinaDataset(filename_csv='./namestotal1.csv', index='test_data_target.csv',
                                            image_path=PATH)

        self.Dataset_gesund = RetinaDataset(filename_csv='./namestotal1.csv', index='train_gesund_target.csv',
                                            image_path=PATH)
        self.Dataset_krank = RetinaDataset(filename_csv='./namestotal1.csv', index='train_krank_target.csv',
                                           image_path=PATH)

        self.train_loader_krank = DataLoader(self.Dataset_krank, batch_size=self.batchsize, num_workers=0, shuffle=True)
        self.train_loader_gesund = DataLoader(self.Dataset_gesund, batch_size=self.batchsize, num_workers=0,
                                              shuffle=True)

        self.validate_loader_t = DataLoader(self.valset_target, batch_size=1, num_workers=0,
                                        shuffle=False)
        self.test_loader_t = DataLoader(self.Testset_target, batch_size=1, num_workers=0,
                                        shuffle=False)
        my_list = ['c1', 'c2']
        params = list(filter(lambda kv: kv[0] in my_list, self.netD.parameters()))
        base_params = list(filter(lambda kv: kv[0] not in my_list, self.netD.parameters()))
        print(len(base_params),len(params) )
        param_groups = [
            {'params': params, 'lr': .001},
            {'params': base_params, 'lr': .0001},
        ]
        self.d_optimizer = torch.optim.Adam(param_groups,  weight_decay=1e-5)
        self.d_optimizer.zero_grad()
        self.netD = torch.nn.DataParallel(self.netD, device_ids=[id], output_device=id)

        def weights_init_k(m):
            if isinstance(m, nn.Conv3d):
                nn.init.kaiming_normal_(m.weight.data, mode='fan_out', nonlinearity='relu')

                if m.bias is not None:
                    torch.nn.init.zeros_(m.bias)

        self.netD.apply(weights_init_k)

# =================================================================================== #
#                             1. Preprocess input data                                #
# =================================================================================== #

    def train(self):
       early_stopping = EarlyStopping(patience=20, verbose=True, path='./results/trgtonly_classaware_line1.pt')

       for epoch in range(2000):
        print('START')

        self.netD=self.netD.train()
        running_loss = 0; total=0; correct=0
        sum_loss_cls = 0;sum_latent = 0; sum_org=0

        for i, datag in enumerate(self.train_loader_gesund):
            x_real_g = datag['image'];
            label_g = datag['labels']
            label_gesund=torch.zeros(len(label_g))
            y = iter(self.train_loader_krank)
            x_real_k=y.next()['image']; label_k=y.next()['labels']
            x_real=torch.cat((x_real_g,x_real_k),dim=0)
            label_krank = torch.ones(len(label_k))
            label_org=torch.cat((label_gesund, label_krank), dim=0).long()

            self.netD.train()
            k = i

            x_real=get_patch(x_real)
            x_real=np.array(visualize_ch(x_real));

            x_real[0:1]=apply_augmentation(x_real[0:1])
            x_real[5:6]=apply_augmentation(x_real[5:6])

            x_real = torch.tensor(x_real).to(device)
            noise = torch.rand(x_real.shape).to(device)*0.05
            x_real = standardize_ch(x_real+noise).to(device)
            label_org = label_org.to(device)
            num_it = round(len(self.Dataset_gesund) * epoch / self.batchsize + i, 0)
            inputs = Variable(x_real, requires_grad=True)

            if (i+1)%10==0:


                     self.image_window1 = viz.image(visualize(x_real[0, 0,45, :, :]), win=self.image_window1,
                                               opts=dict(caption="reconstructed1 label"+ str(label_org[0].item())))
                     self.image_window2 = viz.heatmap(visualize(x_real[0,0, :,45, :]), win=self.image_window2,
                                               opts=dict(caption="reconstructed2"))
                     self.image_window3 = viz.image(visualize(x_real[0, 0, :, :,60]), win=self.image_window3,
                                               opts=dict(caption="reconstructed3"))

# =================================================================================== #
#                             2. Train the model
# =================================================================================== #


            out_cls,latent,c1new, c2new = self.netD(inputs)
            d_loss_cls = classification_loss(logit=out_cls, target=label_org)
            err_latent=0
            err_original=0
            d_loss = self.lambda_cls_d * d_loss_cls
            self.d_optimizer.zero_grad()

            torch.autograd.set_detect_anomaly(True)
            d_loss.backward()

            ave_grads = []
            for n, p in self.netD.named_parameters():
                 if (p.requires_grad) and ("bias" not in n) and p.grad is not None:
                     ave_grads.append(p.grad.abs().mean())
            gradd = sum(ave_grads)
            nn.utils.clip_grad_norm_(self.netD.parameters(), 50)


            self.d_optimizer.step()

     # Logging.
            loss = {}
            loss['D/loss_cls'] = d_loss_cls.item();
            sum_loss_cls += d_loss_cls.item(); sum_latent  +=err_latent; sum_org +=err_original

            running_loss += d_loss
           # Plot loss
            if (k + 1) % 2 == 0:
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_loss_cls]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_cls',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_latent]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_latent',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_org]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_original',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([running_loss]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_total',
                          update='append')

                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([gradd]).unsqueeze(0).cpu(), win=self.grad_window,
                         name='grad_d', update='append')

                running_loss = 0;
                sum_loss_cls = 0; sum_latent=0; sum_org=0

            if (k + 1) % self.n_critic == 0:
                 _, predicted = torch.max(out_cls.data, 1);
                 total += 2*self.batchsize+2
                 correct += (predicted == label_org).sum().item()
                 accuracy = 100 * correct / total;


# =================================================================================== #
#                                 4. Miscellaneous                                    #
# =================================================================================== #
            if (num_it+1) %50== 0:

                    viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                             win=self.val_window,
                             name='accuracy train', update='append')
                    correct = 0;
                    total = 0
                    self.netD = self.netD.eval()

                    with torch.no_grad():
                        long_pred = torch.zeros(0).long()
                        long_cls = torch.zeros(0).long()
                        long_score = torch.zeros(0)
                        val_loss = 0
                        for i, data in enumerate(self.validate_loader_t):
                            x_real = data['image'];
                            label_org2 = data['labels']
                            x_real = np.array(visualize(x_real[..., 20:-20, 20:-20, 20:-20])) * 255
                            p2 = np.percentile(x_real, 2)
                            p98 = np.percentile(x_real, 98)
                            x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                            x_real = visualize_ch(torch.tensor(x_real)).to(device)
                            x_real = standardizetensor(x_real)
                            out_cls, latent, _, _ = self.netD(x_real)
                            val_loss_item = classification_loss(out_cls.cpu(), label_org2.cpu())
                            val_loss += val_loss_item
                            y_score = out_cls[:, 1].cpu()
                            _, predicted = torch.max(out_cls.data, 1);
                            total += 1
                            correct += (predicted.cpu() == label_org2.cpu()).sum().item()
                            long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                            long_cls = torch.cat((long_cls, label_org2), dim=0)
                            long_score = torch.cat((long_score, y_score), dim=0)
                            accuracy = 100 * correct / total
                        print('Accuracy test: %d %%' % (accuracy))
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                                 win=self.val_window,
                                 name='accuracy target', update='append')
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([val_loss]).unsqueeze(0).cpu(),
                                 win=self.lossd_window,
                                 name='val loss', update='append')
                        early_stopping(val_loss, self.netD)

                        (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                        print('kappa', kappa)
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                                 win=self.val_window,
                                 name='kappa target', update='append')

                        auc = roc_auc_score(long_cls, long_score)
                        print(auc)
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                                 win=self.val_window,
                                 name='roc target', update='append')

                        correct = 0;
                        total = 0

            self.netD.train
            if early_stopping.early_stop:
                    print("Early stopping")
                    break

# =================================================================================== #
#                                 5. Testing                         #
# =================================================================================== #

    def test(self, load_file=None):
                if load_file is not None:
                    self.netD.load_state_dict(torch.load(load_file))
                    print("loaded model from file")

                correct = 0;
                total = 0
                self.netD = self.netD.eval()
                with torch.no_grad():
                    long_pred = torch.zeros(0).long()
                    long_cls = torch.zeros(0).long()
                    long_score = torch.zeros(0)
                    for i, data in enumerate(self.test_loader_t):
                        x_real = data['image'];
                        label_org2 = data['labels']
                        x_real = np.array(visualize(x_real[..., 20:-20, 20:-20, 20:-20])) * 255
                        p2 = np.percentile(x_real, 2)
                        p98 = np.percentile(x_real, 98)
                        x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                        x_real = visualize_ch(torch.tensor(x_real)).to(device)
                        x_real = standardizetensor(x_real)

                        out_cls, latent, _, _ = self.netD(x_real)
                        out_cls = nnf.softmax(out_cls)

                        y_score = out_cls[:, 1].cpu()

                        _, predicted = torch.max(out_cls.data, 1);
                        total += 1
                        correct += (predicted.cpu() == label_org2.cpu()).sum().item()
                        long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                        long_cls = torch.cat((long_cls, label_org2), dim=0)
                        long_score = torch.cat((long_score, y_score), dim=0)
                        accuracy = 100 * correct / total
                    print('Accuracy test: %d %%' % (accuracy))
                    (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                    print('kappa', kappa)
                    auc = roc_auc_score(long_cls, long_score)
                    print('target auc', auc, 'acc', correct / total)

if __name__ == '__main__':
    solver = Solver()
    solver.train()
