import torch
from skimage import exposure
import matplotlib as mpl
from torch.utils.data import Dataset,  DataLoader
import pandas as pd
import copy
import time
from pytorchtools import EarlyStopping
from random import uniform
from crossvalidation import RetinaDataset
from volumentations import *
import os
os.environ['OMP_NUM_THREADS'] = '8'
os.environ['CUDA_VISIBLE_DEVICES'] ='0'

torch.set_num_threads(8)
mpl.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as nnf
import nibabel as nib
from torch.nn import functional as F
import scipy
from sklearn.metrics import roc_auc_score
from visdom import Visdom
from losses import NewLoss_only,NewLoss_latent
viz = Visdom(port=8850)
from mhatonumpy import  normalize, visualize_ch, visualize, imshow, standardize, \
    classification_loss, kappa_score,get_patch, padtosize, stretch_batch,standardizetensor, normalize_ch, apply_augmentation, standardize_ch

import torch.nn as nn
from torch.autograd import Variable
import random

from inception_resnet import InceptionResnetV1
torch.manual_seed(3)
torch.cuda.manual_seed_all(3)
np.random.seed(3)
random.seed(3)

id = 0

ACTIVATION = nn.ReLU()
c_dim = 2

torch.cuda.set_device(id)
device = 'cuda'
print("computations done on ", device)

class Solver(object):
    def __init__(self, path, ablation=None):
        super(Solver, self).__init__()
        self.num_latent=128
        PATH=path
        self.ablation=ablation

        self.positive = F.normalize(torch.ones(self.num_latent)[:, None], dim=0).to(device)
        self.negative =  F.normalize(-torch.ones(self.num_latent)[:, None], dim=0).to(device)
        c1 = Variable(self.positive, requires_grad=True)
        c2 = Variable(self.negative, requires_grad=True)
        self.netD=InceptionResnetV1(flag='update_fullyconnected').to(device)

        p2 = np.array([np.array(p.shape).prod() for p in self.netD.parameters()]).sum()
        print('number of parameters',  p2)

        blank = np.ones((256, 256))
        self.lossd_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                opts=dict(xlabel='epoch', ylabel='Loss', title='training loss '))

        self.image_window1 = viz.image(blank);self.image_window2 = viz.image(blank);self.image_window3 = viz.image(blank)
        self.image_window4 = viz.image(blank);self.image_window5 = viz.image(blank);self.image_window6 = viz.image(blank)
        self.image_window7 = viz.image(blank);self.image_window8 = viz.image(blank);self.image_window9 = viz.image(blank)

        self.grad_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                               opts=dict(xlabel='epoch', ylabel='gradient', title='average gradients'))
        self.val_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                              opts=dict(xlabel='epoch', ylabel='accuracy', title='classification accuracy on validation set'))
        self.test_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                              opts=dict(xlabel='epoch', ylabel='accuracy', title='classification accuracy on test set'))

        self.latent_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                 opts=dict(xlabel='epoch', ylabel='Loss', title='latent 0'))
        self.latent_window2 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                 opts=dict(xlabel='epoch', ylabel='Loss', title='latent 1'))
        self.latent_window3 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                       opts=dict(xlabel='epoch', ylabel='Loss', title='latent c1'))
        self.latent_window4 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                       opts=dict(xlabel='epoch', ylabel='Loss', title='latent c2'))

        self.batchsize =10
        self.loss_fnonly = NewLoss_only()
        self.loss_fn_latent=NewLoss_latent()
        self.lambda_cls_d = 1
        self.lambda_latent=1
        self.lambda_org=100
        self.n_critic = 1

        self.Dataset =  RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'training_data.csv',
                                         image_path=PATH)
        self.train_loader  = DataLoader(self.Dataset, batch_size=self.batchsize, num_workers=0,
                                 shuffle=True)
        self.valset_source = RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'validation_data_source.csv',
                                         image_path=PATH)
        self.valset_target =  RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'validation_data_target.csv',
                                         image_path=PATH)
        self.Testset_target =  RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'test_data_target.csv',
                                         image_path=PATH)
        self.Testset_source =  RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'test_data_source.csv',
                                         image_path=PATH)
        self.validate_loader_s = DataLoader(self.valset_source, batch_size=1, num_workers=0,
                         shuffle=False)

        self.validate_loader_t = DataLoader(self.valset_target, batch_size=1, num_workers=0,
                         shuffle=False)

        self.test_loader_t = DataLoader(self.Testset_target, batch_size=1, num_workers=0,
                         shuffle=False)
        self.test_loader_s = DataLoader(self.Testset_source, batch_size=1, num_workers=0,
                         shuffle=False)
        self.Cset =  RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'train_gesund_target.csv',
                                         image_path=PATH)
        self.c_loader =DataLoader(self.Cset, batch_size=1, num_workers=0,
                         shuffle=True)
        self.Pset =  RetinaDataset(filename_csv=PATH+'namestotal1.csv', index=PATH+'train_krank_target.csv',
                                         image_path=PATH)
        self.p_loader = DataLoader(self.Pset, batch_size=1, num_workers=0,
                         shuffle=True)
        my_list = ['c1', 'c2']
        params = list(filter(lambda kv: kv[0] in my_list, self.netD.parameters()))
        base_params = list(filter(lambda kv: kv[0] not in my_list, self.netD.parameters()))
        print(len(base_params),len(params) )
        param_groups = [
            {'params': params, 'lr': .0001},
            {'params': base_params, 'lr': .00005},
        ]
        self.d_optimizer = torch.optim.Adam(param_groups,  weight_decay=1e-5)
        self.d_optimizer.zero_grad()
        self.netD = torch.nn.DataParallel(self.netD, device_ids=[id], output_device=id)

        def weights_init_k(m):
            if isinstance(m, nn.Conv3d):
                nn.init.kaiming_normal_(m.weight.data, mode='fan_out', nonlinearity='relu')

                if m.bias is not None:
                    torch.nn.init.zeros_(m.bias)

        self.netD.apply(weights_init_k)


 # =================================================================================== #
 #                             1. Preprocess input data                                #
 # =================================================================================== #

    def train(self):
       if self.ablation == 'Fixed':
            name='./results/fixed_line1.pt'
       elif self.ablation == 'd2r0':
            name = './results/d2r0_line1.pt'
       elif self.ablation == None:
            name = './results/L2I_line1.pt'
       early_stopping = EarlyStopping(patience=20, verbose=True, path=name)

       x = np.arange(0, self.num_latent, 1)
       torch.cuda.synchronize()
       a = time.perf_counter()

       for epoch in range(2000):
        print('START')

        self.netD=self.netD.train()
        running_loss = 0; total=0; correct=0
        sum_loss_cls = 0;sum_latent = 0; sum_org=0

        for i, data in enumerate(self.train_loader):
            self.netD.train()
            x_real=data['image']; label_org=data['labels']
            k = i
            dataiterp = iter(self.p_loader);dataiterc = iter(self.c_loader)
            beispielp= dataiterp.next()['image']; beispielc = dataiterc.next()['image']

            x_real=get_patch(x_real)
            beispielc=get_patch(beispielc);beispielp=get_patch(beispielp)

            x_real=np.array(visualize_ch(x_real))
            beispielp=np.array(visualize_ch(beispielp))
            beispielc=np.array(visualize_ch(beispielc))

            x_real[0:1]=apply_augmentation(x_real[0:1])
            beispielp=apply_augmentation(beispielp)
            beispielc=apply_augmentation(beispielc)

            x_real = torch.tensor(x_real[...])
            beispielc = torch.tensor(beispielc)
            beispielp = torch.tensor(beispielp)

            noise = torch.rand(x_real.shape)*0.05;noise2 = torch.rand(beispielp.shape)*0.05;noise3 = torch.rand(beispielc.shape)*0.05

            x_real = standardize_ch(x_real+noise).to(device)
            vergleich_c = standardizetensor(beispielc+noise2).to(device)
            vergleich_p = standardizetensor(beispielp+noise3).to(device)

            label_org = label_org.to(device)
            num_it = round(len(self.Dataset) * epoch / self.batchsize + i, 0)
            inputs = Variable(x_real, requires_grad=True)

# =================================================================================== #
#                             2. Train the model                      #
 # =================================================================================== #

            _,latent_p,_,_ = self.netD(vergleich_p)
            _,latent_c ,_,_= self.netD(vergleich_c)

            out_cls,latent,c1new,c2new= self.netD(inputs)
            if self.ablation == 'Fixed':
                c1new = self.positive;
                c2new = self.negative
                print('fixed')

            if self.ablation== 'd2r0':
                radius=0; distance=2
                print('d2r0')
            else:
                radius=0.1;distance=1.9

            d_loss_cls2=classification_loss(out_cls,label_org)

            c=0
            for l in range(len(label_org)):
                if label_org[l] == 0:
                   f1 = self.loss_fn_latent(latent[l], c1new.detach(), c2new.detach(), target=1, r=radius, dist=distance)
                else:
                   f1 = self.loss_fn_latent(latent[l], c1new.detach(), c2new.detach(), target=0, r=radius, dist=distance)

                c+=f1
            err_latent = c

            err_original=self.loss_fnonly(latent_c, latent_p,c1new, c2new , r=radius, dist=distance)

           # Backward and optimize.
            d_loss =d_loss_cls2+err_latent*self.lambda_latent+self.lambda_org*err_original

            self.d_optimizer.zero_grad();
            d_loss.backward()
            ave_grads = []
            gradd = []
            for n, p in self.netD.named_parameters():
                 if (p.requires_grad) and ("bias" not in n) and p.grad is not None:
                     ave_grads.append(p.grad.abs().mean())
            gradd = sum(ave_grads)
            nn.utils.clip_grad_norm_(self.netD.parameters(), 50)

            self.d_optimizer.step()
            sum_loss_cls += d_loss_cls2.item(); sum_latent  +=err_latent; sum_org +=err_original
            running_loss += d_loss.detach()

     # Plot loss
            if (k + 1) % 10 == 0:

                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_loss_cls]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_cls',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_latent]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_latent',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_org]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_original',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([running_loss]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_total',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([gradd]).unsqueeze(0).cpu(), win=self.grad_window,
                         name='grad_d', update='append')

                running_loss = 0;
                sum_loss_cls = 0; sum_latent=0; sum_org=0

            if (k+ 1) % self.n_critic == 0:
                 _, predicted = torch.max(out_cls.data, 1);
                 total += 1 * (self.batchsize)
                 correct += (predicted == label_org).sum().item()
                 accuracy = 100 * correct / total;


# =================================================================================== #
#                                 4. Miscellaneous                                    #
# =================================================================================== #
            if (num_it+1) %50== 0:

                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(), win=self.val_window,
                                       name='accuracy train', update='append')
                correct=0; total=0

                self.netD = self.netD.eval()

                with torch.no_grad():
                     long_pred = torch.zeros(0).long()
                     long_cls = torch.zeros(0).long()
                     long_score = torch.zeros(0)
                     val_loss=0
                     for i,data in enumerate(self.validate_loader_t):
                         x_real = data['image']
                         label_org2 = data['labels']
                         x_real = np.array(visualize(x_real[...,20:-20,20:-20,20:-20])) * 255
                         p2 = np.percentile(x_real, 2)
                         p98 = np.percentile(x_real, 98)
                         x_real= exposure.rescale_intensity(x_real, in_range=(p2, p98))
                         x_real = torch.tensor(x_real).to(device)
                         x_real = standardizetensor(x_real)
                         out_cls, latent,c1,c2= self.netD(x_real)
                         out_cls = nnf.softmax(out_cls)
                         dist=distance
                         distance_c = torch.sqrt((c1 - c2).pow(2).sum())
                         val_loss_item = classification_loss(out_cls.cpu(), label_org2.cpu())+self.loss_fn_latent(latent.cpu(), c1.cpu(), c2.cpu(), target=abs(1-label_org2.cpu()), r=radius, dist=distance)+F.relu(dist-distance_c).cpu()
                         val_loss += val_loss_item

                         y_score = out_cls[:, 1].cpu()
                         _, predicted = torch.max(out_cls.data, 1);
                         total += 1
                         correct += (predicted.cpu() == label_org2.cpu()).sum().item()
                         long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                         long_cls = torch.cat((long_cls, label_org2), dim=0)
                         long_score = torch.cat((long_score, y_score), dim=0)
                     accuracy = 100 * correct / total
                     print('Accuracy target: %d %%' % (accuracy))
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                              win=self.val_window,
                              name='accuracy test', update='append')
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([val_loss]).unsqueeze(0).cpu(),
                              win=self.lossd_window,
                              name='val loss', update='append')
                     early_stopping(val_loss, self.netD)

                     (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                     print('kappa', kappa)
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                              win=self.val_window,
                              name='kappa target', update='append')

                     auc = roc_auc_score(long_cls, long_score)
                     print(auc)
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                              win=self.val_window,
                              name='roc target', update='append')

                     long_pred = torch.zeros(0).long()
                     long_cls = torch.zeros(0).long()
                     long_score = torch.zeros(0)
                     correct=0; total=0

                     for i, data in enumerate(self.validate_loader_s):
                             x_real3 = data['image'];
                             label_org3 = data['labels']
                             x_real = np.array(visualize(x_real3[...,20:-20,20:-20,20:-20])) * 255
                             p2 = np.percentile(x_real, 2)
                             p98 = np.percentile(x_real, 98)
                             x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                             x_real = torch.tensor(x_real).to(device)
                             x_real = standardizetensor(x_real)

                             out_cls, latent,_,_ = self.netD(x_real)
                             out_cls = nnf.softmax(out_cls)
                             y_score = out_cls[:, 1].cpu()

                             _, predicted = torch.max(out_cls.data, 1);
                             total += 1
                             correct += (predicted.cpu() == label_org3.cpu()).sum().item()
                             long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                             long_cls = torch.cat((long_cls, label_org3), dim=0)
                             long_score = torch.cat((long_score, y_score), dim=0)
                             accuracy = 100 * correct / total
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                                  win=self.val_window,
                                  name='accuracy val', update='append')

                     (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                                  win=self.val_window,
                                  name='kappa source', update='append')

                     auc = roc_auc_score(long_cls, long_score)
                     print(auc)
                     viz.line(X=torch.ones((1, 1)).cpu() * num_it,
                                      Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                                      win=self.val_window,
                                      name='roc source', update='append')

                     correct=0; total=0


                     self.netD = self.netD.eval()


                if early_stopping.early_stop:
                    print("Early stopping")
                    break
            if early_stopping.early_stop:
                print("Early stopping")
                break
        if early_stopping.early_stop:
            print("Early stopping")
            break

    # # =================================================================================== #
    # #                                 5. Testing                         #
    # # =================================================================================== #


    def test(self, load_file=None):
            if load_file is not None:
                self.netD.load_state_dict(torch.load(load_file))
                print("loaded model from file")

            i = 0
            correct = 0;
            total = 0

            self.netD = self.netD.eval()
            with torch.no_grad():
                long_pred = torch.zeros(0).long()
                long_cls = torch.zeros(0).long()
                long_score = torch.zeros(0)
                for i, data in enumerate(self.test_loader_t):
                    x_real = data['image'];
                    label_org2 = data['labels']
                    x_real = np.array(visualize(x_real[..., 20:-20, 20:-20, 20:-20])) * 255
                    p2 = np.percentile(x_real, 2)
                    p98 = np.percentile(x_real, 98)
                    x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                    x_real = torch.tensor(x_real).to(device)
                    x_real = standardizetensor(x_real)
                    out_cls, latent, _, _ = self.netD(x_real)
                    out_cls = nnf.softmax(out_cls)

                    y_score = out_cls[:, 1].cpu()

                    _, predicted = torch.max(out_cls.data, 1);
                    total += 1
                    correct += (predicted.cpu() == label_org2.cpu()).sum().item()
                    long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                    long_cls = torch.cat((long_cls, label_org2), dim=0)
                    long_score = torch.cat((long_score, y_score), dim=0)
                    accuracy = 100 * correct / total
                print('Accuracy test: %d %%' % (accuracy))
                viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                         win=self.test_window,
                         name='accuracy target', update='append')

                (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                print('kappa', kappa)
                viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                         win=self.test_window,
                         name='kappa target', update='append')

                auc = roc_auc_score(long_cls, long_score)
                print('target a8c', auc, 'acc', correct / total)
                viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                         win=self.test_window,
                         name='roc target', update='append')

                long_pred = torch.zeros(0).long()
                long_cls = torch.zeros(0).long()
                long_score = torch.zeros(0)
                correct = 0;
                total = 0

                for i, data in enumerate(self.test_loader_s):
                    x_real3 = data['image'];
                    label_org3 = data['labels']
                    x_real = np.array(visualize(x_real3[..., 20:-20, 20:-20, 20:-20])) * 255
                    p2 = np.percentile(x_real, 2)
                    p98 = np.percentile(x_real, 98)
                    x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                    x_real = torch.tensor(x_real).to(device)
                    x_real = standardizetensor(x_real)

                    out_cls, latent, _, _ = self.netD(x_real)
                    out_cls = nnf.softmax(out_cls)

                    y_score = out_cls[:, 1].cpu()

                    _, predicted = torch.max(out_cls.data, 1);
                    total += 1
                    correct += (predicted.cpu() == label_org3.cpu()).sum().item()
                    long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                    long_cls = torch.cat((long_cls, label_org3), dim=0)
                    long_score = torch.cat((long_score, y_score), dim=0)
                    accuracy = 100 * correct / total
                viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                         win=self.test_window,
                         name='accuracy source', update='append')

                print('long', long_cls, long_score)
                (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                         win=self.test_window,
                         name='kappa  source', update='append')

                auc = roc_auc_score(long_cls, long_score)
                print('source auc', auc, 'kappa', kappa, 'acc', accuracy)
                viz.line(X=torch.ones((1, 1)).cpu() * 1,
                         Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                         win=self.test_window,
                         name='roc source', update='append')


if __name__ == '__main__':

    solver=Solver()
    solver.train()
