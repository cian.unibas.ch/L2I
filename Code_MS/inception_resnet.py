import torch
from torch import nn
from torch.nn import functional as F


class BasicConv3d(nn.Module):

    def __init__(self, in_planes, out_planes, kernel_size, stride, padding=0):
        super().__init__()
        self.conv = nn.Conv3d(
            in_planes, out_planes,
            kernel_size=kernel_size, stride=stride,
            padding=padding, bias=False
        )
        self.bn = nn.InstanceNorm3d(num_features=out_planes)
        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


class Block35(nn.Module):

    def __init__(self, scale=1.0):
        super().__init__()

        self.scale = scale

        self.branch0 = BasicConv3d(256, 32, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv3d(256, 32, kernel_size=1, stride=1),
            BasicConv3d(32, 32, kernel_size=3, stride=1, padding=1)
        )

        self.branch2 = nn.Sequential(
            BasicConv3d(256, 32, kernel_size=1, stride=1),
            BasicConv3d(32, 32, kernel_size=3, stride=1, padding=1),
            BasicConv3d(32, 32, kernel_size=3, stride=1, padding=1)
        )

        self.conv3d = nn.Conv3d(96, 256, kernel_size=1, stride=1)
        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        out = torch.cat((x0, x1, x2), 1)
        out = self.conv3d(out)
        out = out * self.scale + x
        out = self.relu(out)
        return out


class Block17(nn.Module):

    def __init__(self, scale=1.0):
        super().__init__()

        self.scale = scale

        self.branch0 = BasicConv3d(896, 128, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv3d(896, 128, kernel_size=1, stride=1),
            BasicConv3d(128, 128, kernel_size=(1, 7, 1), stride=1, padding=(0, 3, 0)),
            BasicConv3d(128, 128, kernel_size=(7, 1, 1), stride=1, padding=(3, 0, 0)),
            BasicConv3d(128, 128, kernel_size=(1, 1, 7), stride=1, padding=(0, 0, 3))

        )

        self.conv3d = nn.Conv3d(256, 896, kernel_size=1, stride=1)
        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        out = torch.cat((x0, x1), 1)
        out = self.conv3d(out)
        out = out * self.scale + x
        out = self.relu(out)
        return out


class Block8(nn.Module):

    def __init__(self, scale=1.0, noReLU=False):
        super().__init__()

        self.scale = scale
        self.noReLU = noReLU

        self.branch0 = BasicConv3d(1792, 192, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv3d(1792, 192, kernel_size=1, stride=1),
            BasicConv3d(192, 192, kernel_size=(1, 3, 1), stride=1, padding=(0, 1, 0)),
            BasicConv3d(192, 192, kernel_size=(3, 1, 1), stride=1, padding=(1, 0, 0)),
            BasicConv3d(192, 192, kernel_size=(1, 1, 3), stride=1, padding=(0, 0, 1))

        )

        self.conv3d = nn.Conv3d(384, 1792, kernel_size=1, stride=1)
        if not self.noReLU:
            self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        out = torch.cat((x0, x1), 1)
        out = self.conv3d(out)
        out = out * self.scale + x
        if not self.noReLU:
            out = self.relu(out)
        return out


class Mixed_6a(nn.Module):

    def __init__(self):
        super().__init__()

        self.branch0 = BasicConv3d(256, 384, kernel_size=3, stride=2)

        self.branch1 = nn.Sequential(
            BasicConv3d(256, 192, kernel_size=1, stride=1),
            BasicConv3d(192, 192, kernel_size=3, stride=1, padding=1),
            BasicConv3d(192, 256, kernel_size=3, stride=2)
        )

        self.branch2 = nn.MaxPool3d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        out = torch.cat((x0, x1, x2), 1)
        return out


class Mixed_7a(nn.Module):

    def __init__(self):
        super().__init__()

        self.branch0 = nn.Sequential(
            BasicConv3d(896, 256, kernel_size=1, stride=1),
            BasicConv3d(256, 384, kernel_size=3, stride=2)
        )

        self.branch1 = nn.Sequential(
            BasicConv3d(896, 256, kernel_size=1, stride=1),
            BasicConv3d(256, 256, kernel_size=3, stride=2)
        )

        self.branch2 = nn.Sequential(
            BasicConv3d(896, 256, kernel_size=1, stride=1),
            BasicConv3d(256, 256, kernel_size=3, stride=1, padding=1),
            BasicConv3d(256, 256, kernel_size=3, stride=2)
        )

        self.branch3 = nn.MaxPool3d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)
        out = torch.cat((x0, x1, x2, x3), 1)
        return out


class InceptionResnetV1(nn.Module):
    """Inception Resnet V1 model with optional loading of pretrained weights.
    Model parameters can be loaded based on pretraining on the VGGFace2 or CASIA-Webface
    datasets. Pretrained state_dicts are automatically downloaded on model instantiation if
    requested and cached in the torch cache. Subsequent instantiations use the cache rather than
    redownloading.
    Keyword Arguments:
        pretrained {str} -- Optional pretraining dataset. Either 'vggface2' or 'casia-webface'.
            (default: {None})
        classify {bool} -- Whether the model should output classification probabilities or feature
            embeddings. (default: {False})
        num_classes {int} -- Number of output classes. If 'pretrained' is set and num_classes not
            equal to that used for the pretrained model, the final linear layer will be randomly
            initialized. (default: {None})
        dropout_prob {float} -- Dropout probability. (default: {0.6})
    """

    def __init__(self, flag, pretrained=None, classify=True, num_classes=2, dropout_prob=0.2, device=None):
        super().__init__()

        # Set simple attributes
        self.flag=flag
        self.pretrained = pretrained
        self.classify = classify
        self.num_classes = num_classes
        self.num_latent =128
        self.c1 = torch.nn.Parameter(2*torch.ones(self.num_latent))
        self.c2 = torch.nn.Parameter(-2*torch.ones(self.num_latent))

        if pretrained == 'vggface2':
            tmp_classes = 8631
        elif pretrained == 'casia-webface':
            tmp_classes = 10575
        elif pretrained is None and self.classify and self.num_classes is None:
            raise Exception('If "pretrained" is not specified and "classify" is True, "num_classes" must be specified')

        # Define layers
        self.conv3d_1a = BasicConv3d(1, 32, kernel_size=3, stride=2)
        self.conv3d_2a = BasicConv3d(32, 32, kernel_size=3, stride=1)
        self.conv3d_2b = BasicConv3d(32, 64, kernel_size=3, stride=1, padding=1)
        self.maxpool_3a = nn.MaxPool3d(3, stride=2)
        self.conv3d_3b = BasicConv3d(64, 80, kernel_size=1, stride=1)
        self.conv3d_4a = BasicConv3d(80, 192, kernel_size=3, stride=1)
        self.conv3d_4b = BasicConv3d(192, 256, kernel_size=3, stride=2)
        self.repeat_1 = nn.Sequential(
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
            Block35(scale=0.17),
        )
        self.mixed_6a = Mixed_6a()
        self.repeat_2 = nn.Sequential(
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
            Block17(scale=0.10),
        )
        self.mixed_7a = Mixed_7a()
        self.repeat_3 = nn.Sequential(
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
            Block8(scale=0.20),
        )
        self.block8 = Block8(noReLU=True)
        self.avgpool_1a = nn.AdaptiveAvgPool3d(1)

        self.dropout = nn.Dropout(dropout_prob)

        self.last_linear = nn.Sequential(nn.Linear(1792, 1024, bias=False),
                                         nn.LeakyReLU(),
                                         nn.Dropout(p=0.1),
                                         nn.Linear(1024, 128))

        if self.classify and self.num_classes is not None:
            self.logits = nn.Sequential(
                nn.LeakyReLU(),
                nn.Dropout(p=0.1),
                nn.Linear(128, self.num_classes))

        self.device = torch.device('cpu')
        if device is not None:
            self.device = device
            self.to(device)

    def forward(self, x):
        """Calculate embeddings or logits given a batch of input image tensors.
        Arguments:
            x {torch.tensor} -- Batch of image tensors representing faces.
        Returns:
            torch.tensor -- Batch of embedding vectors or multinomial logits.

        """

        c1 = self.c1
        c2 = self.c2
        x = self.conv3d_1a(x)
        x = self.conv3d_2a(x)
        x = self.conv3d_2b(x)
        x = self.maxpool_3a(x)
        x = self.conv3d_3b(x)
        x = self.conv3d_4a(x)
        x = self.conv3d_4b(x)
        x = self.repeat_1(x)
        x = self.mixed_6a(x)

        x = self.repeat_2(x)
        x = self.mixed_7a(x)
        x = self.repeat_3(x)
        x = self.block8(x)

        x = self.avgpool_1a(x)

        x = self.dropout(x)
        x = x.view(x.shape[0], -1)

        x = self.last_linear(x)
        x = F.normalize(x, p=2, dim=1)
        latent0 = x.clone()

        if self.flag== 'update_all':
            y=x
        elif self.flag == 'update_fullyconnected':
            y=x.detach()

        if self.classify:
            y = self.logits(y.view(x.shape[0], -1))
        else:
            x = F.normalize(x, p=2, dim=1)
        return y, latent0.view(latent0.size(0), -1), c1, c2

