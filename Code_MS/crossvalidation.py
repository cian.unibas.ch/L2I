from __future__ import print_function, division
import os
import torch
import nibabel as nib
from torch.utils.data import Dataset,  DataLoader
from PIL import Image, ImageOps
import PIL
import numpy as np
import pandas as pd
from mhatonumpy import stretch_batch, visualize
import numpy as np
from sklearn.model_selection import train_test_split
import random
import csv

def nifti_loader(path):
    sample = nib.load(path)
    sample2=torch.from_numpy(np.asarray(sample.dataobj).astype(dtype = 'float32'))
    sample2=sample2[None,...]
    return sample2

def load_csv_file(filename: str, header_elements=None):

    with open(filename, newline='', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.reader(csvfile)
        csv_header = None
        csv_idx = None

        images = []
        for idx, row in enumerate(csv_reader):
            if idx == 0:
                csv_header = row

                if header_elements is not None:
                    new_csv_header = ["ID"]
                    for landmark in header_elements:
                        new_csv_header.append(landmark)
                csv_idx = []

                if header_elements is not None:
                    for idx_2, element_header in enumerate(csv_header):
                        if element_header in new_csv_header:
                            csv_idx.append(idx_2)

                    csv_header = new_csv_header
            else:
                if header_elements is not None:
                    filtered_row = []
                    for idx in csv_idx:
                            filtered_row.append(row[idx])

                            images.append(filtered_row)
                else:
                    images.append(row)

        return images, csv_header



def save_csv_file(landmarks, csv_header, fielename: str) -> None:

    with open(fielename, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        csv_writer.writerow(csv_header)

        for idx, landmark in enumerate(landmarks):
            csv_writer.writerow(landmark)


class RetinaDataset(Dataset):
    def __init__(self, filename_csv, index,  image_path, numrow=1, index2=None):

        self._filename_csv = filename_csv
        self._image_path = image_path

        self._data_points, self._csv_header = load_csv_file(self._filename_csv)
        with open(index, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            row = next(spamreader)
            row2 = next(spamreader)
            row3 = next(spamreader)
            row4 = next(spamreader)
            row5 = next(spamreader)
            row6 = next(spamreader)
            row7 = next(spamreader)
            row8 = next(spamreader)
            row9 = next(spamreader)

        # load the data from the csv file
        if numrow==1:
            selected_row=row
            if len(row)<3:
                selected_row=row[0].split(",")
        elif numrow==2:
            selected_row = row2[0].split(",")
        elif numrow == 3:
            selected_row = row3[0].split(",")
        elif numrow == 4:
            selected_row = row4[0].split(",")
        elif numrow == 5:
            selected_row = row5[0].split(",")
        elif numrow == 6:
            selected_row = row6[0].split(",")
        elif numrow == 7:
            selected_row = row7[0].split(",")
        elif numrow ==8:
            selected_row = row8[0].split(",")
        elif numrow ==9:
            selected_row = row9[0].split(",")

        if index2 is not None:
            with open(index2, newline='') as csvfile:
                spamreader2 = csv.reader(csvfile, delimiter=' ', quotechar='|')
                in2row = next(spamreader2)
                in2row2 = next(spamreader2)
                inrow3 = next(spamreader2)
                inrow4 = next(spamreader2)
                inrow5 = next(spamreader2)
                inrow6 = next(spamreader2)
                inrow7 = next(spamreader2)
                inrow8 = next(spamreader2)
                inrow9 = next(spamreader2)
            if numrow==1:
                selected_row2=in2row
                if len(in2row)<3:
                    selected_row2=in2row[0].split(",")
            elif numrow==2:
                selected_row2 = in2row2[0].split(",")
            elif numrow == 3:
                selected_row2 = inrow3[0].split(",")
            elif numrow ==4:
                selected_row2 = inrow4[0].split(",")
            elif numrow == 5:
                selected_row2 = inrow5[0].split(",")
            elif numrow == 6:
                selected_row2 = inrow6[0].split(",")
            elif numrow == 7:
                selected_row2 = inrow7[0].split(",")
            elif numrow == 8:
                selected_row2 = inrow8[0].split(",")
            elif numrow == 9:
                selected_row2 = inrow9[0].split(",")
            selected_row=selected_row+selected_row2
            print('selected', len(selected_row))
            print('selected2', len(selected_row2))

        reader = self._data_points
        self.results = list(filter(lambda row: row[0]  in  selected_row, reader))
        self._data_points=self.results
        for idx, element in enumerate(self._data_points):
              self._data_points[idx] = element


    @property
    def csv_header(self):
        return self._csv_header

    @property
    def number_of_images(self):
        return len(self._data_points)


    def __len__(self):
        return len(self._data_points)

    def __getitem__(self, idx):

            image_filename=self._image_path+self._data_points[idx][-1][1:]
            image=np.array(nifti_loader(image_filename))
            image=np.array(visualize(image)) * 255
            image = stretch_batch(image)

            labels = self._data_points[idx][2]
            weight= self._data_points[idx][4]
            labels=np.array(labels, dtype=np.long)
            weight=np.array(weight, dtype=np.float32)
            return {"image": image, "labels": labels, "weight": weight}

