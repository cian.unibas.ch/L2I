import torch
from skimage import exposure
from crossvalidation import RetinaDataset
from pytorchtools import EarlyStopping
import time
import os
os.environ['OMP_NUM_THREADS'] = '4'
os.environ['CUDA_VISIBLE_DEVICES'] ='0'
from torch.utils.data import Dataset,  DataLoader
torch.set_num_threads(4)
import numpy as np
import torch.nn.functional as nnf
from sklearn.metrics import roc_auc_score
from visdom import Visdom
viz = Visdom(port=8850)
from mhatonumpy import   visualize_ch, visualize, \
  get_patch, classification_loss, kappa_score,  standardizetensor, apply_augmentation, standardize_ch
import torch.nn as nn
from torch.autograd import Variable
import random
from inception_resnet import InceptionResnetV1

torch.manual_seed(1)
torch.cuda.manual_seed_all(1)
np.random.seed(1)
random.seed(1)

id = 0
ACTIVATION = nn.ReLU()
c_dim = 2
torch.cuda.set_device(id)
device = 'cuda'
print("computations done on ", device)



class Solver(object):
    def __init__(self, path, ablation=None):
        super(Solver, self).__init__()
        self.num_latent=128
        self.netD=InceptionResnetV1(flag='update_all').to(device)

        PATH = path
        self.ablation = ablation

        p2 = np.array([np.array(p.shape).prod() for p in self.netD.parameters()]).sum()
        print('number of parameters',  p2)
        blank = np.ones((256, 256))
        self.lossd_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                opts=dict(xlabel='epoch', ylabel='Loss', title='training loss discriminator'))

        self.image_window1 = viz.image(blank);self.image_window2 = viz.image(blank);self.image_window3 = viz.image(blank)

        self.grad_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                               opts=dict(xlabel='epoch', ylabel='gradient', title='average gradients'))
        self.val_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                              opts=dict(xlabel='epoch', ylabel='accuracy', title='classification accuracy on validation set'))
        self.test_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                              opts=dict(xlabel='epoch', ylabel='accuracy', title='classification accuracy on test set'))

        self.latent_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                 opts=dict(xlabel='epoch', ylabel='Loss', title='latent 0'))
        self.latent_window2 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                 opts=dict(xlabel='epoch', ylabel='Loss', title='latent 1'))
        self.latent_window3 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                       opts=dict(xlabel='epoch', ylabel='Loss', title='latent c1'))
        self.latent_window4 = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                                       opts=dict(xlabel='epoch', ylabel='Loss', title='latent c2'))

        self.batchsize =10
        self.lambda_cls_d = 1
        self.lambda_latent=1
        self.lambda_org=100

        beta1 = 0.5
        beta2 = 0.999
        self.n_critic = 1


        self.Dataset = RetinaDataset(filename_csv='./namestotal1.csv', index='train_gesund_target.csv',
                                  image_path=PATH, index2='train_krank_target.csv')
        self.train_loader = DataLoader(self.Dataset, batch_size=self.batchsize, num_workers=0,shuffle=True)

        self.valset_target = RetinaDataset(filename_csv='./namestotal1.csv', index='validation_data_target.csv',
                                           image_path=PATH)
        self.Testset_target = RetinaDataset(filename_csv='./namestotal1.csv', index='test_data_target.csv',
                                            image_path=PATH)


        self.validate_loader_t = DataLoader(self.valset_target, batch_size=1, num_workers=0,
                                            shuffle=False)

        self.test_loader_t = DataLoader(self.Testset_target, batch_size=1, num_workers=0,
                                        shuffle=True)


        self.d_optimizer = torch.optim.Adam(self.netD.parameters(), 0.00005, [beta1, beta2],weight_decay=1e-5)
        self.d_optimizer.zero_grad()

        self.netD = torch.nn.DataParallel(self.netD, device_ids=[id], output_device=id)


        def weights_init_k(m):
            if isinstance(m, nn.Conv3d):
                nn.init.kaiming_normal_(m.weight.data, mode='fan_out', nonlinearity='relu')

                if m.bias is not None:
                    torch.nn.init.zeros_(m.bias)

        self.netD.apply(weights_init_k)

 # =================================================================================== #
 #                             1. Preprocess input data                                #
 # =================================================================================== #

    def train(self):
       if self.ablation == 'weighted':
            name='./results/targetonly_weighted_line1.pt'
       elif self.ablation == None:
            name='./results/targetonly_line1.pt'

       early_stopping = EarlyStopping(patience=20, verbose=True, path=name)
       for epoch in range(2000):
        print('START')
        self.netD=self.netD.train()
        running_loss = 0; total=0; correct=0
        sum_loss_cls = 0;sum_latent = 0; sum_org=0

        for i, data in enumerate(self.train_loader):
            x_real = data['image'];
            label_org = data['labels']
            tf=np.array(label_org, dtype=bool)
            weight=torch.ones(label_org.size())*55/71
            weight[tf]= 16/71
            self.netD.train();

            k = i
            x_real=get_patch(x_real)
            x_real=np.array(visualize_ch(x_real))
            x_real[0:1] = apply_augmentation(x_real[0:1])

            x_real = torch.tensor(x_real).to(device)
            noise = torch.rand(x_real.shape).to(device)*0.05
            x_real = standardize_ch(x_real+noise).to(device)

            label_org = label_org.to(device)
            num_it = round(len(self.Dataset) * epoch / self.batchsize + i, 0)
            inputs = Variable(x_real, requires_grad=True)

            self.image_window1 = viz.image(visualize(x_real[0, 0,45, :, :]), win=self.image_window1,
                                               opts=dict(caption="reconstructed1 label"+ str(label_org[0].item())))
            self.image_window2 = viz.heatmap(visualize(x_real[0,0, :,45, :]), win=self.image_window2,
                                               opts=dict(caption="reconstructed2"))
            self.image_window3 = viz.image(visualize(x_real[0, 0, :, :,60]), win=self.image_window3,
                                               opts=dict(caption="reconstructed3"))

# =================================================================================== #
#                             2. Train the model                           #
# =================================================================================== #

            out_cls,latent,_,_= self.netD(inputs)

            err_latent = 0
            if self.ablation=='weighted':
                print('weighted')
                d_loss_cls_long = classification_loss(logit=out_cls, target=label_org)
                d_loss_cls = (d_loss_cls_long * weight.to(device)).sum()
            else:
                d_loss_cls = classification_loss(logit=out_cls, target=label_org)

            err_original=0
            d_loss=d_loss_cls
            self.d_optimizer.zero_grad();

            d_loss.backward()
            ave_grads = []
            for n, p in self.netD.named_parameters():
                 if (p.requires_grad) and ("bias" not in n) and p.grad is not None:
                     ave_grads.append(p.grad.abs().mean())
            gradd = sum(ave_grads)
            nn.utils.clip_grad_norm_(self.netD.parameters(), 50)
            self.d_optimizer.step()
            b = time.perf_counter()

            loss = {}
            loss['D/loss_cls'] = d_loss_cls.item();
            sum_loss_cls += d_loss_cls.detach().item(); sum_latent  +=err_latent; sum_org +=err_original
            running_loss += d_loss.detach()
            torch.cuda.empty_cache()

            # Plot loss
            if (k + 1) % 1== 0:

                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_loss_cls]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_cls',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_latent]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_latent',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([sum_org]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_original',
                          update='append')
                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([running_loss]).unsqueeze(0).cpu(),
                          win=self.lossd_window, name='loss_total',
                          update='append')

                viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([gradd]).unsqueeze(0).cpu(), win=self.grad_window,
                         name='grad_d', update='append')

                running_loss = 0;
                sum_loss_cls = 0; sum_latent=0; sum_org=0

            if k  % 1 == 0:
                 _, predicted = torch.max(out_cls.data, 1)
                 total += 1 * (self.batchsize)
                 correct += (predicted == label_org).sum().item()
                 accuracy = 100 * correct / total;


# =================================================================================== #
#                                 4. Miscellaneous                                    #
# =================================================================================== #
            if (num_it+1) % 50 == 0:

                    viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                             win=self.val_window,
                             name='accuracy train', update='append')
                    correct = 0;
                    total = 0
                    self.netD = self.netD.eval()

                    with torch.no_grad():
                        long_pred = torch.zeros(0).long()
                        long_cls = torch.zeros(0).long()
                        long_score = torch.zeros(0)
                        val_loss = 0
                        for i, data in enumerate(self.validate_loader_t):
                            x_real = data['image'];
                            label_org2 = data['labels']
                            x_real = np.array(visualize(x_real[..., 20:-20, 20:-20, 20:-20])) * 255
                            p2 = np.percentile(x_real, 2)
                            p98 = np.percentile(x_real, 98)
                            x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                            x_real = visualize_ch(torch.tensor(x_real)).to(device)
                            x_real = standardizetensor(x_real)
                            out_cls, latent, _, _ = self.netD(x_real)
                            val_loss_item = classification_loss(out_cls.cpu(), label_org2.cpu())
                            val_loss += val_loss_item
                            y_score = out_cls[:, 1].cpu()
                            _, predicted = torch.max(out_cls.data, 1);
                            total += 1
                            correct += (predicted.cpu() == label_org2.cpu()).sum().item()
                            long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                            long_cls = torch.cat((long_cls, label_org2), dim=0)
                            long_score = torch.cat((long_score, y_score), dim=0)
                            accuracy = 100 * correct / total
                        print('Accuracy test: %d %%' % (accuracy))
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                                 win=self.val_window,
                                 name='accuracy target', update='append')
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([val_loss]).unsqueeze(0).cpu(),
                                 win=self.lossd_window,
                                 name='val loss', update='append')
                        early_stopping(val_loss, self.netD)

                        (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                        print('kappa', kappa)
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                                 win=self.val_window,
                                 name='kappa target', update='append')

                        auc = roc_auc_score(long_cls, long_score)
                        print(auc)
                        viz.line(X=torch.ones((1, 1)).cpu() * num_it, Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                                 win=self.val_window,
                                 name='roc target', update='append')

                        correct = 0;
                        total = 0

            self.netD.train
        if early_stopping.early_stop:
            print("Early stopping")
            break

# =================================================================================== #
#                                 5. Testing                         #
# =================================================================================== #

    def test(self, load_file=None):
                if load_file is not None:
                    self.netD.load_state_dict(torch.load(load_file))
                    print("loaded model from file")

                correct = 0;
                total = 0
                self.netD = self.netD.eval()
                with torch.no_grad():
                    long_pred = torch.zeros(0).long()
                    long_cls = torch.zeros(0).long()
                    long_score = torch.zeros(0)
                    for i, data in enumerate(self.test_loader_t):
                        x_real = data['image'];
                        label_org2 = data['labels']
                        x_real = np.array(visualize(x_real[..., 20:-20, 20:-20, 20:-20])) * 255
                        p2 = np.percentile(x_real, 2)
                        p98 = np.percentile(x_real, 98)
                        x_real = exposure.rescale_intensity(x_real, in_range=(p2, p98))
                        x_real = visualize_ch(torch.tensor(x_real)).to(device)
                        x_real = standardizetensor(x_real)

                        out_cls, latent, _, _ = self.netD(x_real)
                        out_cls = nnf.softmax(out_cls)

                        y_score = out_cls[:, 1].cpu()

                        _, predicted = torch.max(out_cls.data, 1);
                        total += 1
                        correct += (predicted.cpu() == label_org2.cpu()).sum().item()
                        long_pred = torch.cat((long_pred, predicted.cpu()), dim=0)
                        long_cls = torch.cat((long_cls, label_org2), dim=0)
                        long_score = torch.cat((long_score, y_score), dim=0)
                        accuracy = 100 * correct / total
                    print('Accuracy test: %d %%' % (accuracy))
                    viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([accuracy]).unsqueeze(0).cpu(),
                             win=self.test_window,
                             name='accuracy target', update='append')

                    print('long', long_cls, long_score)
                    (kappa, upper, lower) = kappa_score(long_pred, long_cls)
                    print('kappa', kappa)
                    viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([kappa * 100]).unsqueeze(0).cpu(),
                             win=self.test_window,
                             name='kappa target', update='append')

                    auc = roc_auc_score(long_cls, long_score)
                    print('target a8c', auc, 'acc', correct / total)
                    viz.line(X=torch.ones((1, 1)).cpu() * 1, Y=torch.Tensor([auc * 100]).unsqueeze(0).cpu(),
                             win=self.test_window,
                             name='roc target', update='append')


if __name__ == '__main__':
    solver = Solver()
    solver.train()
