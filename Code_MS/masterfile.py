import os
import argparse

def main(config):
    method = config.method
    mode=config.mode
    loadfile=config.loadfile
    PATH=config.path

    print('method', method)
    print(method)
    ablation=None
    if method == 'L2I':
            from L2I_3d import Solver
    elif method == 'vanilla':
            from classifier import Solver
    elif method == 'weighted':
            from classifier import Solver
            ablation = 'weighted'
    elif method == 'supcon':
            from classifier_supcon import Solver
    elif method == 'classaware':
            from classifier_aware_sampling import Solver
    elif method=='fixed':
        from L2I_3d import Solver
        ablation = 'Fixed'
    elif method == 'nomargin':
            from L2I_3d import Solver
            ablation = 'd2r0'
    elif method == 'targetonly':
        from classifier_onlytarget import Solver
    elif method == 'targetonly_weighted':
        from classifier_onlytarget import Solver
        ablation = 'weighted'
    elif method == 'targetonly_classaware':
        from classaware_onlytarget import Solver

    if ablation is None:
        solver = Solver(path=PATH)
    else:
        solver = Solver(path=PATH, ablation=ablation)
    if mode== 'train':
        solver.train()
    elif mode=='test':
        print('file', loadfile)
        solver.test(loadfile)


if __name__ == '__main__':
     parser = argparse.ArgumentParser()
     print(os.getcwd())

     parser.add_argument('--method', type=str, default='vanilla', help='model to train')
     parser.add_argument('--mode', type=str, default='train', help='train or test')
     parser.add_argument('--loadfile', type=str, default=None, help='load pretrained model')
     parser.add_argument('--path', type=str, default=None, help='load pretrained model')

     config = parser.parse_args()
     print(config)
     main(config)

