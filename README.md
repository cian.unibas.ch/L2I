

# Learn to Ignore

This repository is the official implementation of our MICCAI2022 submission *Learn to Ignore: Domain Adaptation for Multi-Site MRI Analysis*. We trained and evaluated our method *L2I* and all comparing methods on a dataset of 3D brain MR images of Multiple Sclerosis (MS) patients and healthy controls. As this dataset cannot be shared due to data privacy concerns, we implemented it as well for the 2D [Office31](https://people.eecs.berkeley.edu/~jhoffman/domainadapt/) dataset, for binary classification between phones and speakers.

# Requirements
To install requirements:
```setup
pip install -r requirements.txt
```

## Datasets
The MS dataset used in the paper cannot be shared due to data privacy concerns, as it includes patient data. 
The Office31 dataset can be found in the folder *officetotal*. On the Office31 dataset, the task is to distinguish between phones and speakers. The target domain is given by images aquired with a webcam, while the source domain contains images from the *Amazon* webshop.

All methods were trained for 10 runs. For each run, the data was randomly split into the training, validation and test set. A list of all images with the given indices can be found in the file *Code_Office31/namestotal2d.csv*. The list of the image indices for each run for training, validation and test sets can be found in the remaining csv files.


  
# Training
The 3D implementation for the MS dataset is in the folder *Code_MS*. The code for the Office31 dataset can be found in the folder *Code_Office31*.  To run the code for the chosen dataset, go into the corresponding folder. The trained models are then saved into the folder *Code_MS/results* or *Code_Office31/results* respectively. For training on the Office31 dataset, the option ```--path datapath``` can be omitted. For the MS dataset, it should indicate the path to the directory where the data is stored.

### 1. L2I
To train our model *L2I*, run the following command:
```train
python3 masterfile.py --method L2I --mode train --path datapath
```
This is the implementation of our idea presented in the paper, with the hyperparameters given in the paper.



### 2. Fixed
This is an ablation study of our method *L2I*, where the center points are fixed. As we want to compare the methods, the hyperparameters are the same as for the method L2I, which are provided in the paper.
To train the model, run this command:
```train
python3 masterfile.py --method fixed --mode train --path datapath
```

### 3. No-Margin
This is an ablation study of our method *L2I*: We choose the margins d=2 and r=0. As we want to compare the methods, the hyperparameters are the same as for the method *L2I*, which are provided in the paper.
To train the model, run this command:
```train
python3 masterfile.py --method nomargin --mode train --path datapath
```


### 4. Vanilla

This is the *Vanilla* classification network following the implementation of Inception-Resnet given in this Github repo: https://github.com/timesler/facenet-pytorch/blob/master/models/inception_resnet_v1.py (MIT License). But only L<sub>cls</sub> is taken to update
the parameters of both the encoder and classifier. As we want to compare the methods, the hyperparameters are the same as for the method L2I, which are provided in the paper.
To train the model on both the source and the target domain, run this command:
```train
python3 masterfile.py --method vanilla --mode train --path datapath
```
To train the model on the target dataset only, run this command:
```train
python3 masterfile.py --method targetonly --mode train --path datapath
```


### 5. Weighted
We train the Vanilla classifier with L<sub>cls</sub> and introduce weights to the loss function to compensate for class imbalances. When training on the target domain alone, we group the input images according to their class, resulting in two groups. When training on both the source and the target domain, we group the input images according to their domain and class, resulting in four groups. The loss is weighted according to the inverted proportion of the group in the whole training set.
As we want to compare the methods, the hyperparameters are the same as for the method *L2I*, which are provided in the paper.
To train the model on both the source and the target domain, run this command:
```train
python3 masterfile.py --method weighted --mode train --path datapath
```

If you want to train on the target dataset only, run
```train
python3 masterfile.py --method targetonly_weighted --mode train --path datapath
```


### 6. Class-aware
We train the Vanilla classifier with L<sub>cls</sub> and use class-aware sampling. When training on the target domain only, we sample such that in every batch, there are 5 images for each class.
When training on both the source and the target domain, we make sure that in every batch, there is at least one example from both classes and both domains. We sample in a way that in every batch, there are 5 images from the source domain for each class, and 1 image from the target domain for each class. This is similar to the sampling scheme of the CAN and the Unlearning method. The *Vanilla* classifier is trained with class-aware sampling. As we want to compare the methods, the hyperparameters are the same as for the method *L2I*, which are provided in the paper.
To train the model on the whole dataset, run this command:
```train
python3 masterfile.py --method classaware --mode train --path datapath
```
If you want to train on the target dataset only, run
```train
python3 masterfile.py --method targetonly_classaware --mode train --path datapath
```


### 7. DANN
To train the method *DANN*, run
```
python3 ./DANN/train/main.py --path datapath
```
The implementation of the DANN method is taken from https://github.com/fungtion/DANN (MIT License). The classification network was changed to [Inception-Resnet v1](https://github.com/timesler/facenet-pytorch/blob/master/models/inception_resnet_v1.py). We changed the data loading scheme and adapted the convolutional layers to 3D inputs for the MS dataset. Further, we chose the batch size=10, and the learning rate=0.0001.


### 8. Unlearning
To train the method *Unlearning* on the Office31 dataset, run
```
python3 ./Unlearning/normal_biased.py --epochs 400 --batchsize 10 --patience 100 --epoch_stage_1 200 
```
For training on the MS dataset, choose the options ```--epochs 1000 --batchsize 10 --patience 100 --epoch_stage_1 500 --path datapath```. All the other hyperparameters are kept the same as in the official Github repo on https://github.com/nkdinsdale/Unlearning_for_MRI_harmonisation (No license found). For the Office31 dataset, the 3D Convolutions were changed to 2D Convolutions. The data loading files were also adapted to our needs.


### 9. Contrastive
To train the method *Contrastive*, run
```train
python3 masterfile.py --method supcon --mode train --path datapath
```
The implementation of the supervised contrastive loss is taken from https://github.com/HobbitLong/SupContrast (BSD 2-Clause "Simplified" License).
The hyperparameters are chosen the same as in *L2I* for comparison.

### 10. CAN
We follow the official Github implementation on https://github.com/kgl-prml/Contrastive-Adaptation-Network-for-Unsupervised-Domain-Adaptation (Apache License 2.0). The data loader had to be changed. For the MS dataset, the convolutions were changed to 3D convolutions. The chosen hyperparameters can be found in the file *./CAN/config/config.py*.
To train the method *CAN* on the Office31 dataset, run the following command:
```
 python3 ./CAN/tools/train.py --cfg ./CAN/experiments/config/Office-31/CAN/office31_twoclasses.yaml --method CAN --exp_name office31_twoclasses          
```
To train the method *CAN* on the MS dataset, run the following command. The option ```--path datapath``` should indicate the path to the directory where the data is stored.
```
python3 ./CAN/tools/train.py --cfg ./CAN/experiments/config/Office-31/CAN/office31_twoclasses3d.yaml --method CAN --exp_name ms-line1  --path datapath
```


## Evaluation
In the following table, the commands to evaluate all methods on the test set are presented. The option ``` --path datapath``` can be omitted when evaluating the Office31 dataset.

| Model name         | Command | 
| ------------------ |---------------- | 
|L2I   |    ```python3 masterfile.py --method L2I --mode test  --loadfile filename --path datapath ```    |   
|Fixed |    ```python3  masterfile.py --method fixed --mode test  --loadfile filename --path datapath ```     |   
|No-marging |    ```python3  masterfile.py --method nomargin --mode test --loadfile filename --path datapath ```     |   
|Vanilla  |    ```python3 masterfile.py --method vanilla --mode test --loadfile filename --path datapath ``` | 
|Vanilla only on target dataset|    ```python3 masterfile.py --method targetonly --mode test --loadfile filename --path datapath ```  | 
|Weighted  |   ```python3  masterfile.py --method weighted --mode test --loadfile filename --path datapath ```     |   
|Weighted only on target dataset|    ```python3 masterfile.py --method targetonly_weighted --mode test --loadfile filename --path datapath ```  | 
|Class-aware |    ```python3  masterfile.py --method classaware --mode test --loadfile filename --path datapath ```     |
|Class-aware only on target dataset|    ```python3 masterfile.py --method targetonly_classaware --mode test --loadfile filename --path datapath ```  | 
|DANN |    ```python3 ./DANN/train/test.py --loadfile filename --path datapath```     |  
|Unlearning |    ```python3 ./Unlearning/test_unlearning.py --loadfile filepath --path datapath```     |   
|Contrastive |    ```python3  masterfile.py --method supcon --mode test --loadfile filename --path datapath ```     |   
|CAN Office31 |    ``` python3 ./CAN/tools/test.py --cfg ./CAN/experiments/config/Office-31/CAN/office31_twoclasses.yaml --method CAN --exp_name office31_twoclasses --loadfile filename               ```   | 
|CAN MS |    ```python3 ./CAN/tools/test.py --cfg ./CAN/experiments/config/Office-31/CAN/office31_twoclasses3d.yaml --exp_name ms-line1 --method CAN --loadfile filename --path datapath   ```   | 


## Contributing
Apache 2.0 License
