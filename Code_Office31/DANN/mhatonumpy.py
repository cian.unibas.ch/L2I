import torch
import numpy as np
import nibabel as nib
import torch.nn.functional as F
from sklearn.metrics import confusion_matrix
from torch.utils.checkpoint import checkpoint_sequential
import torch.utils.checkpoint as checkpoint
import SimpleITK as sitk
from skimage import exposure
from PIL import Image
import torchvision
import torchvision.transforms as transforms
from visdom import Visdom
import matplotlib.pyplot as plt
import PIL.Image
import torchvision.transforms.functional as TF
from scipy import misc
import os
from skimage import exposure
import numpy as np
import scipy.ndimage.morphology
import torch.nn as nn
import random
import torch.optim as optim
from scipy import ndimage
from torchio.transforms import Compose as Compose_torchio
vis=Visdom()

def visualize(img):
    _min = img.min()
    _max = img.max()
    normalized_img = (img - _min)/ (_max - _min)
    return normalized_img

def jpg_loader(path):
    sample=Image.open(path)
   #
    data = np.asarray(sample),
    return sample

def __str__(self):
    return 'str'

def standardize(img):
    mean = np.mean(img)
    std = np.std(img)
    img = (img - mean) / std
    return img

def standardize_ch(img):
    normalized_img=torch.zeros(img.shape)
    for k in range(len(img)):

         mean = img[k].mean()
         std= img[k].std()
         normalized_img[k] = (torch.tensor(img[k]) - mean) / (std)
    return normalized_img


def standardizetensor(img):
    mean =img.mean()
    std = img.std()
    img = (img - mean) / std
    return img

def classification_loss(logit, target):
        """Compute binary or softmax cross entropy loss."""
        return torch.nn.functional.cross_entropy(logit, target)

def kappa_score(preds1, preds2):
    cnf = confusion_matrix(preds1, preds2)
    row_marg = np.sum(cnf, axis=1)
    col_marg = np.sum(cnf, axis=0)
    marg_mult = col_marg * row_marg
    n = np.sum(row_marg)
    pr_e = np.sum(marg_mult) / n / n
    pr_a = (cnf[0][0] + cnf[1][1]) / n
    kappa = (pr_a - pr_e) / (1 - pr_e)

    se_k = (pr_a * (1 - pr_a)) / (n * (1 - pr_e) ** 2)
    lower = kappa - 1.96 * se_k
    upper = kappa + 1.96 * se_k
    return kappa, lower, upper


def stretch_batch(img):
    normalized_img=torch.zeros(img.shape)

    for k in range(len(img)):
        p2 = np.percentile(img[k], 2)
        p98 = np.percentile(img[k], 98)
        normalized_img[k] = torch.tensor(exposure.rescale_intensity(img[k], in_range=(p2, p98)))
    return normalized_img


def normalize(img):

    _min = img.min()
    _max = img.max()

  #  normalized_img = (img - _min)/ (_max - _min)
    normalized_img = 2*(img - _min) / (_max - _min)-1
    return normalized_img

def normalize_ch(img):
    normalized_img=torch.zeros(img.shape)
    for k in range(len(img)):

         _min = img[k].min()
         _max = img[k].max()
         normalized_img[k] = 2*(img[k] - _min) / (_max - _min)-1
    return normalized_img


def visualize_ch(img):
    normalized_img=torch.zeros(img.shape)
    for k in range(len(img)):

         _min = img[k].min()
         _max = img[k].max()
         normalized_img[k] = (torch.tensor(img[k]) - _min) / (_max - _min)
    return normalized_img



def get_patch_2d(img):
    newimg = torch.zeros(len(img), 3,236,236)
    for k in range(len(img)):
        zufall1 = np.random.randint(5,15);
        zufall2 = np.random.randint(5,15);

        newimg[k] = visualize_ch(img[k, :, zufall1:zufall1 +236, zufall2:zufall2 + 236]) * 255
    return np.array(newimg)


def get_patch(img):
    newimg = torch.zeros(len(img), 1,124,120,172)
    for k in range(len(img)):
        zufall1 = np.random.randint(5,35);
        zufall2 = np.random.randint(5,35);
        zufall3 = np.random.randint(5, 35);
        newimg[k] = visualize_ch(img[k, :, zufall1:zufall1 +124, zufall2:zufall2 + 120,zufall3:zufall3 + 172]) * 255
    return np.array(newimg)
