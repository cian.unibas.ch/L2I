# Nicola Dinsdale 2020
# Pytorch dataset for numpy arrays
########################################################################################################################
# Import dependencies
import torch
import torchvision.transforms as transforms
import sys
import PIL
from PIL import Image
import os
Path2 = os.path.abspath('../..')
Path = os.path.abspath('.')
import csv
sys.path.append("../..")
from torch.utils.data import Dataset
import torchvision
import nibabel as nib
import numpy as np
from visdom import Visdom
viz = Visdom(port=8851)

blank = np.ones((256, 256))

########################################################################################################################

def jpg_loader(path):
    sample=Image.open(path)
    data = np.asarray(sample)
    return sample


def load_csv_file(filename: str, header_elements=None):

    with open(filename, newline='', encoding='utf-8-sig') as csvfile:
        csv_reader = csv.reader(csvfile)
        csv_header = None
        csv_idx = None

        images = []
        for idx, row in enumerate(csv_reader):
            if idx == 0:
                csv_header = row

                if header_elements is not None:
                    new_csv_header = ["ID"]
                    for landmark in header_elements:
                        new_csv_header.append(landmark)
                csv_idx = []

                if header_elements is not None:
                    for idx_2, element_header in enumerate(csv_header):
                        if element_header in new_csv_header:
                            csv_idx.append(idx_2)

                    csv_header = new_csv_header
            else:
                if header_elements is not None:
                    filtered_row = []
                    for idx in csv_idx:
                            filtered_row.append(row[idx])

                            images.append(filtered_row)
                else:
                    images.append(row)

        return images, csv_header

augmentation_transform = transforms.Compose([
    transforms.RandomRotation(degrees=[-2, 2]),
    transforms.RandomAffine(degrees=2, translate=(0.05, 0.05), scale=(0.9, 1.1), resample=PIL.Image.BILINEAR),
    transforms.Resize(256),
    transforms.ToTensor(),
])

augmentation_transform2 = transforms.Compose([
    transforms.Resize(256),
    transforms.ToTensor(),
])

def visualize_ch(img):
    normalized_img=torch.zeros(img.shape)
    for k in range(len(img)):

         _min = img[k].min()
         _max = img[k].max()
         normalized_img[k] = (torch.tensor(img[k]) - _min) / (_max - _min)
    return normalized_img

def get_patch_2d(img):
    newimg = torch.zeros(len(img), 3,236,236)
    for k in range(len(img)):
        zufall1 = np.random.randint(5,15);
        zufall2 = np.random.randint(5,15);
        newimg[k] = visualize_ch(img[k, :, zufall1:zufall1 +236, zufall2:zufall2 + 236]) * 255
    return np.array(newimg)

class numpy_dataset(Dataset):  # Inherit from Dataset class
    def __init__(self, index, index2=None, filename_csv=Path + '/namestotal2d.csv', numrow=10, val=False, transform=None):
        self._filename_csv = filename_csv
        self.transform = transform

        self._data_points, self._csv_header = load_csv_file(self._filename_csv)
        with open(index, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            row = next(spamreader)
            row2 = next(spamreader)
            row3 = next(spamreader)
            row4 = next(spamreader)
            row5 = next(spamreader)
            row6 = next(spamreader)
            row7 = next(spamreader)
            row8 = next(spamreader)
            row9 = next(spamreader)
            row10 = next(spamreader)


        # load the data from the csv file
        if numrow == 1:
            selected_row = row
            if len(row) < 3:
                selected_row = row[0].split(",")
        elif numrow == 2:
            selected_row = row2[0].split(",")
        elif numrow == 3:
            selected_row = row3[0].split(",")
        elif numrow == 4:
            selected_row = row4[0].split(",")
        elif numrow == 5:
            selected_row = row5[0].split(",")
        elif numrow == 6:
            selected_row = row6[0].split(",")
        elif numrow == 7:
            selected_row = row7[0].split(",")
        elif numrow == 8:
            selected_row = row8[0].split(",")
        elif numrow == 9:
            selected_row = row9[0].split(",")
        elif numrow == 10:
            selected_row = row10[0].split(",")


        if index2 is not None:
            with open(index2, newline='') as csvfile:
                spamreader2 = csv.reader(csvfile, delimiter=' ', quotechar='|')
                in2row = next(spamreader2)
                in2row2 = next(spamreader2)
                inrow3 = next(spamreader2)
                inrow4 = next(spamreader2)
                inrow5 = next(spamreader2)
                inrow6 = next(spamreader2)
                inrow7 = next(spamreader2)
                inrow8 = next(spamreader2)
                inrow9 = next(spamreader2)
                inrow10 = next(spamreader2)

            if numrow == 1:
                selected_row2 = in2row
                if len(in2row) < 3:
                    selected_row2 = in2row[0].split(",")
            elif numrow == 2:
                selected_row2 = in2row2[0].split(",")
            elif numrow == 3:
                selected_row2 = inrow3[0].split(",")
            elif numrow == 4:
                selected_row2 = inrow4[0].split(",")
            elif numrow == 5:
                selected_row2 = inrow5[0].split(",")
            elif numrow == 6:
                selected_row2 = inrow6[0].split(",")
            elif numrow == 7:
                selected_row2 = inrow7[0].split(",")
            elif numrow == 8:
                selected_row2 = inrow8[0].split(",")
            elif numrow == 9:
                selected_row2 = inrow9[0].split(",")
            elif numrow == 10:
                selected_row2 = inrow10[0].split(",")

            selected_row = selected_row + selected_row2
            print('selected', len(selected_row))
        path2 = self._data_points[1][-1]

        reader = self._data_points
        self.results = list(filter(lambda row: row[0] in selected_row, reader))
        self._data_points = self.results
        for idx, element in enumerate(self._data_points):
            self._data_points[idx] = element
        D = torch.zeros((len(self._data_points), 2))
        print('index', index)
        if 'target' in index:
            D[:,0]=1
        else:
            D[:, 1]= 1

        self.targetvector=D

    @property
    def csv_header(self):
        return self._csv_header

    @property
    def number_of_images(self):
        return len(self._data_points)

    def __len__(self):
        return len(self._data_points)

    def __getitem__(self, index):
        zufall1 = np.random.randint(5,15);
        zufall2 = np.random.randint(5,15);

        image_filename = self._data_points[index][-1]
        image_filename = os.path.join(Path2, image_filename[2:])

        x = jpg_loader(image_filename)
        if self.transform==None:
            x = augmentation_transform2(x)
        else:
            x = augmentation_transform(x)

        d=self.targetvector[index]

        x = x[:,zufall1:zufall1 +236, zufall2:zufall2 + 236]
        noise = torch.rand(x.shape) * 0.05
        x=x+noise

        if self.transform:
            x = self.transform(x)

        return x, d

    def __len__(self):

        return len(self._data_points)


class numpy_dataset_three(Dataset):
    def __init__(self, index, index2=None, filename_csv=Path+'/namestotal2d.csv',
                      numrow=10, val=False, transform=None):

        self._filename_csv = filename_csv
        self.transform = transform

        self._data_points, self._csv_header = load_csv_file(self._filename_csv)
        with open(index, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=' ', quotechar='|')
            row = next(spamreader)
            row2 = next(spamreader)
            row3 = next(spamreader)
            row4 = next(spamreader)
            row5 = next(spamreader)
            row6 = next(spamreader)
            row7 = next(spamreader)
            row8 = next(spamreader)
            row9 = next(spamreader)
            row10 = next(spamreader)

        # load the data from the csv file
        if numrow == 1:
            selected_row = row
            if len(row) < 3:
                selected_row = row[0].split(",")
        elif numrow == 2:
            selected_row = row2[0].split(",")
        elif numrow == 3:
            selected_row = row3[0].split(",")
        elif numrow == 4:
            selected_row = row4[0].split(",")
        elif numrow == 5:
            selected_row = row5[0].split(",")
        elif numrow == 6:
            selected_row = row6[0].split(",")
        elif numrow == 7:
            selected_row = row7[0].split(",")
        elif numrow == 8:
            selected_row = row8[0].split(",")
        elif numrow == 9:
            selected_row = row9[0].split(",")
        elif numrow == 10:
            selected_row = row10[0].split(",")

        if index2 is not None:
            with open(index2, newline='') as csvfile:
                spamreader2 = csv.reader(csvfile, delimiter=' ', quotechar='|')
                in2row = next(spamreader2)
                in2row2 = next(spamreader2)
                inrow3 = next(spamreader2)
                inrow4 = next(spamreader2)
                inrow5 = next(spamreader2)
                inrow6 = next(spamreader2)
                inrow7 = next(spamreader2)
                inrow8 = next(spamreader2)
                inrow9 = next(spamreader2)
                inrow10 = next(spamreader2)

            if numrow == 1:
                selected_row2 = in2row
                if len(in2row) < 3:
                    selected_row2 = in2row[0].split(",")
            elif numrow == 2:
                selected_row2 = in2row2[0].split(",")
            elif numrow == 3:
                selected_row2 = inrow3[0].split(",")
            elif numrow == 4:
                selected_row2 = inrow4[0].split(",")
            elif numrow == 5:
                selected_row2 = inrow5[0].split(",")
            elif numrow == 6:
                selected_row2 = inrow6[0].split(",")
            elif numrow == 7:
                selected_row2 = inrow7[0].split(",")
            elif numrow == 8:
                selected_row2 = inrow8[0].split(",")
            elif numrow == 9:
                selected_row2 = inrow9[0].split(",")
            elif numrow == 10:
                selected_row2 = inrow10[0].split(",")

            selected_row = selected_row + selected_row2
            print('selected', len(selected_row))
        path2 = self._data_points[1][-1]

        reader = self._data_points
        self.results = list(filter(lambda row: row[0] in selected_row, reader))
        self._data_points = self.results

        D = torch.zeros((len(self._data_points), 2))

        for idx, element in enumerate(self._data_points):
            self._data_points[idx] = element
        if 'target' in index:
            D[:,0]=1
        else:
            D[:, 1]= 1

        self.targetvector=D#torch.cat((d1,d2), dim=0)


    @property
    def csv_header(self):
        return self._csv_header

    @property
    def number_of_images(self):
        return len(self._data_points)

    def __len__(self):
        return len(self._data_points)


    def __getitem__(self, index):

        zufall1 = np.random.randint(5,15);zufall2 = np.random.randint(5,15);


        image_filename = self._data_points[index][-1]
        image_filename = os.path.join(Path2, image_filename[2:])

        x = jpg_loader(image_filename)
        if self.transform==None:
            x = augmentation_transform2(x)
        else:
            x = augmentation_transform(x)


        x = x[:,zufall1:zufall1 +236, zufall2:zufall2 + 236]
        noise = torch.rand(x.shape) * 0.05
        x=torch.tensor(x)+noise
        y = self._data_points[index][2]
        y = np.array(y, dtype=np.long)

        d = self.targetvector[index]


        return x, y, d

    def __len__(self):
        return len(self._data_points)

