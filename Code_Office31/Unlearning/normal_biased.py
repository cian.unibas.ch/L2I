# Nicola Dinsdale 2020
# Unlearning with biased datasets dataset
########################################################################################################################
from models.age_predictor import DomainPredictor, Regressor, Encoder
from datasets.numpy_dataset import numpy_dataset_three, numpy_dataset
from losses2.confusion_loss import confusion_loss

from torch.utils.data import DataLoader
import torch
import torch.nn as nn
from visdom import Visdom
viz = Visdom(port=8850)
import numpy as np
import sys
import os

from sklearn.utils import shuffle
from utils import Args, EarlyStopping_unlearning
import torch.optim as optim
from train_utils import train_unlearn_distinct, val_unlearn_distinct, val_encoder_domain_unlearn_distinct, train_encoder_domain_unlearn_distinct
import argparse

parser = argparse.ArgumentParser()
print(os.getcwd())
parser.add_argument('--epochs', type=int, default=300, help='total number of epochs')
parser.add_argument('--patience', type=int, default=150, help='total number of epochs')
parser.add_argument('--epoch_stage_1', type=int, default=100, help='total number of epochs')
parser.add_argument('--batchsize', type=int, default=16, help='total number of epochs')

config = parser.parse_args()
print('configpat', config.patience)

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
Path = os.path.abspath('../..')

blank = np.ones((256, 256))

image_window1 = viz.image(blank);
image_window2 = viz.image(blank);
image_window3 = viz.image(blank)
lossd_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                             opts=dict(xlabel='epoch', ylabel='Loss', title='training loss discriminator'))
acc_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                             opts=dict(xlabel='epoch', ylabel='acc', title='training acc discriminator'))

########################################################################################################################
# Create an args class
args = Args()
args.channels_first = True
args.epochs = config.epochs
args.batch_size =config.batchsize
args.val_batch_size=1
args.diff_model_flag = False
args.alpha = 1
args.patience = config.patience
args.epoch_stage_1 = config.epoch_stage_1
args.learning_rate = 1e-4

LOAD_PATH_ENCODER =None
LOAD_PATH_REGRESSOR =None
LOAD_PATH_DOMAIN = None

PRE_TRAIN_ENCODER = './results/pretrain_encoder_line10'
PATH_ENCODER = './results/encoder_pth_line10'
CHK_PATH_ENCODER = './results/encoder_chk_pth_line10'
PRE_TRAIN_REGRESSOR = './results/pretrain_regressor_line10'
PATH_REGRESSOR = './results/regressor_pth_line10'
CHK_PATH_REGRESSOR = './results/regressor_chk_pth_line10'
PRE_TRAIN_DOMAIN = './results/pretrain_domain_line10'
PATH_DOMAIN = './results/domain_pth_line10'

PATH_big_Msadv=['./train_mobile_target.csv','./train_speaker_target.csv']
PATH_big_Rest=['./training_amazon_mobile.csv', './training_amazon_speaker.csv']
PATH_big_Rest_val=['./validation_data_source2d.csv']
PATH_big_Msadv_val=['./validation_data_target2d.csv']

CHK_PATH_DOMAIN = 'domain_chk_pth'
LOSS_PATH = 'loss_pth'
id=0
cuda=torch.cuda.set_device(id)

########################################################################################################################
im_size = (256,256)

print('Creating datasets and dataloaders')
b_train_dataset=numpy_dataset_three(index=PATH_big_Rest[0], index2=PATH_big_Rest[1])
b_val_dataset=numpy_dataset_three(index=PATH_big_Rest_val[0],  val=True)
o_train_dataset=numpy_dataset_three(index=PATH_big_Msadv[0], index2=PATH_big_Msadv[1])
o_val_dataset =numpy_dataset_three(index=PATH_big_Msadv_val[0],  val=True)

b_train_dataloader = DataLoader(b_train_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)
b_val_dataloader = DataLoader(b_val_dataset, batch_size=1, shuffle=True, num_workers=0)
o_train_dataloader = DataLoader(o_train_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)
o_val_dataloader = DataLoader(o_val_dataset, batch_size=1, shuffle=True, num_workers=0)

b_int_train_dataset = numpy_dataset(index=PATH_big_Rest[0], index2=PATH_big_Rest[1])
b_int_val_dataset = numpy_dataset(index=PATH_big_Rest_val[0],  val=True)
o_int_train_dataset = numpy_dataset(index=PATH_big_Msadv[0], index2=PATH_big_Msadv[1])
o_int_val_dataset = numpy_dataset(index=PATH_big_Msadv_val[0],  val=True)


b_int_train_dataloader = DataLoader(b_int_train_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)
b_int_val_dataloader = DataLoader(b_int_val_dataset, batch_size=1, shuffle=True, num_workers=0)
o_int_train_dataloader = DataLoader(o_int_train_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)
o_int_val_dataloader = DataLoader(o_int_val_dataset, batch_size=1, shuffle=True, num_workers=0)

# Load in the model
encoder = Encoder()
regressor = Regressor()
domain_predictor = DomainPredictor(nodes=2)

if cuda:
    encoder = encoder.cuda()
    regressor = regressor.cuda()
    domain_predictor = domain_predictor.cuda()

# Make everything parallelisable
encoder = nn.DataParallel(encoder)
regressor = nn.DataParallel(regressor)
domain_predictor = nn.DataParallel(domain_predictor)

if LOAD_PATH_ENCODER:
    print('Loading Weights')
    encoder_dict = encoder.state_dict()
    pretrained_dict = torch.load(LOAD_PATH_ENCODER)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in encoder_dict}
    print('weights loaded encoder = ', len(pretrained_dict), '/', len(encoder_dict))
    encoder.load_state_dict(torch.load(LOAD_PATH_ENCODER))

if LOAD_PATH_REGRESSOR:
    regressor_dict = regressor.state_dict()
    pretrained_dict = torch.load(LOAD_PATH_REGRESSOR)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in regressor_dict}
    print('weights loaded regressor = ', len(pretrained_dict), '/', len(regressor_dict))
    regressor.load_state_dict(torch.load(LOAD_PATH_REGRESSOR))

if LOAD_PATH_DOMAIN:
    domain_dict = domain_predictor.state_dict()
    pretrained_dict = torch.load(LOAD_PATH_DOMAIN)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in domain_dict}
    print('weights loaded domain predictor = ', len(pretrained_dict), '/', len(domain_dict))
    domain_predictor.load_state_dict(torch.load(LOAD_PATH_DOMAIN))

criteron=nn.CrossEntropyLoss()
domain_criterion = nn.BCELoss()
conf_criterion = confusion_loss()
if cuda:
    criteron = criteron.cuda()
    domain_criterion = domain_criterion.cuda()
    conf_criterion = conf_criterion.cuda()

optimizer_step1 = optim.Adam(list(encoder.parameters()) + list(regressor.parameters()) + list(domain_predictor.parameters()), lr=args.learning_rate)
optimizer = optim.Adam(list(encoder.parameters()) + list(regressor.parameters()), lr=1e-4)
optimizer_conf = optim.Adam(list(encoder.parameters()), lr=1e-4)
optimizer_dm = optim.Adam(list(domain_predictor.parameters()), lr=1e-4)         # Lower learning rate for the unlearning bit

# Initalise the early stopping
early_stopping = EarlyStopping_unlearning(args.patience, verbose=False)

epoch_reached = args.epoch_reached  # Change this back to 1
loss_store = []

models = [encoder, regressor, domain_predictor]
optimizers = [optimizer, optimizer_conf, optimizer_dm]
train_dataloaders = [b_train_dataloader, o_train_dataloader, b_int_train_dataloader, o_int_train_dataloader]
val_dataloaders = [b_val_dataloader, o_val_dataloader, b_int_val_dataloader, o_int_val_dataloader]
criterions = [criteron, conf_criterion, domain_criterion]
i=0
for epoch in range(epoch_reached, args.epochs+1):
    if epoch < args.epoch_stage_1:
        i += 1
        print('Training Main Encoder')
        print('Epoch ', epoch, '/', args.epochs, flush=True)
        optimizers = [optimizer_step1]
        loss, acc, dm_loss, conf_loss = train_encoder_domain_unlearn_distinct(args, models, train_dataloaders, optimizers, criterions, epoch)
        torch.cuda.empty_cache()  # Clear memory cache
        val_loss, val_acc, val_acc_target_rest, val_acc_target_msadv,_,_,_,_ = val_encoder_domain_unlearn_distinct(args, models, val_dataloaders, criterions)

        print('val_acc', val_acc_target_msadv)
        loss_store.append([loss, val_loss, acc, val_acc, dm_loss, conf_loss])

        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([loss]).unsqueeze(0).cpu(),win=lossd_window, name='loss',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([dm_loss]).unsqueeze(0).cpu(),win=lossd_window, name='dm_loss',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([conf_loss]).unsqueeze(0).cpu(),win=lossd_window, name='conf_loss',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([acc]).unsqueeze(0).cpu(),win=acc_window, name='acc',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([val_acc]).unsqueeze(0).cpu(),win=acc_window, name='val_acc_domain',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([val_acc_target_rest]).unsqueeze(0).cpu(),win=acc_window, name='val_t_rest',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([val_acc_target_msadv]).unsqueeze(0).cpu(),win=acc_window, name='val_t_msadv',update='append')

        # Save the losses2 each epoch so we can plot them live
        np.save(LOSS_PATH, np.array(loss_store))

        if epoch == args.epoch_stage_1 - 1 or (epoch+1)%200==0:
            torch.save(encoder.state_dict(), PRE_TRAIN_ENCODER)
            torch.save(regressor.state_dict(), PRE_TRAIN_REGRESSOR)
            torch.save(domain_predictor.state_dict(), PRE_TRAIN_DOMAIN)

    else:
        i += 1
        optimizer = optim.Adam(list(encoder.parameters()) + list(regressor.parameters()), lr=1e-5)
        optimizer_conf = optim.Adam(list(encoder.parameters()), lr=1e-5)
        optimizer_dm = optim.Adam(list(domain_predictor.parameters()), lr=1e-5)
        optimizers = [optimizer, optimizer_conf, optimizer_dm]

        print('Unlearning')
        print('Epoch ', epoch, '/', args.epochs, flush=True)

        loss, acc, dm_loss, conf_loss = train_unlearn_distinct(args, models, train_dataloaders, optimizers, criterions, epoch)
        torch.cuda.empty_cache()  # Clear memory cache
        val_loss, val_acc, val_acc_target_rest, val_acc_target_msadv,_,_,_,_ = val_unlearn_distinct(args, models, val_dataloaders, criterions)
        # if val_acc_target_msadv < 1 and val_acc_target_msadv > .9:
        #     torch.save(encoder.state_dict(), PRE_TRAIN_ENCODER)
        #     torch.save(regressor.state_dict(), PRE_TRAIN_REGRESSOR)
        #     torch.save(domain_predictor.state_dict(), PRE_TRAIN_DOMAIN)
        #     sys.exit('rer')
        print('val_acc', val_acc_target_msadv)
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([loss]).unsqueeze(0).cpu(),win=lossd_window, name='loss',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([dm_loss]).unsqueeze(0).cpu(),win=lossd_window, name='dm_loss',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([conf_loss]).unsqueeze(0).cpu(),win=lossd_window, name='conf_loss',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([acc]).unsqueeze(0).cpu(),win=acc_window, name='acc',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([val_acc]).unsqueeze(0).cpu(),win=acc_window, name='val_acc',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([val_acc_target_rest]).unsqueeze(0).cpu(),win=acc_window, name='val_t_rest',update='append')
        viz.line(X=torch.ones((1, 1)).cpu() * i, Y=torch.Tensor([val_acc_target_msadv]).unsqueeze(0).cpu(),win=acc_window, name='val_t_msadv',update='append')



        loss_store.append([loss, val_loss, acc, val_acc, dm_loss, conf_loss])
        np.save(LOSS_PATH, np.array(loss_store))

        # Decide whether the model should stop training or not
        early_stopping(val_loss, models , epoch, optimizer, loss, [CHK_PATH_ENCODER, CHK_PATH_REGRESSOR, CHK_PATH_DOMAIN])
        if early_stopping.early_stop:
            loss_store = np.array(loss_store)
            np.save(LOSS_PATH, loss_store)
            sys.exit('Patience Reached - Early Stopping Activated')

        if epoch == args.epochs:
            print('Finished Training', flush=True)
            print('Saving the model', flush=True)

            # Save the model in such a way that we can continue training later
            torch.save(encoder.state_dict(), PATH_ENCODER)
            torch.save(regressor.state_dict(), PATH_REGRESSOR)
            torch.save(domain_predictor.state_dict(), PATH_DOMAIN)

            loss_store = np.array(loss_store)
            np.save(LOSS_PATH, loss_store)

        torch.cuda.empty_cache()  # Clear memory cache

