# Nicola Dinsdale 2020
# Unlearning with biased datasets dataset
########################################################################################################################
from models.age_predictor import DomainPredictor, Regressor, Encoder
from datasets.numpy_dataset import numpy_dataset_three, numpy_dataset
from torch.utils.data import DataLoader
import torch
import argparse
import torch.nn as nn
from visdom import Visdom
viz = Visdom(port=8852)
import numpy as np
from utils import Args, EarlyStopping_unlearning
from losses2.confusion_loss import confusion_loss
from train_utils import train_unlearn_distinct, val_unlearn_distinct, val_encoder_domain_unlearn_distinct, train_encoder_domain_unlearn_distinct
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

parser = argparse.ArgumentParser()
print(os.getcwd())
parser.add_argument('--loadfile', type=str, default='./results/', help='load pretrained model')
parser.add_argument('--path', type=str, default='.', help='path to data')
parser.add_argument('--numrow', type=str, default='10', help='which run to load')

config = parser.parse_args()
print(config.loadfile)
blank = np.ones((256, 256))

image_window1 = viz.image(blank);
image_window2 = viz.image(blank);
image_window3 = viz.image(blank)
lossd_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                             opts=dict(xlabel='epoch', ylabel='Loss', title='training loss discriminator'))
acc_window = viz.line(Y=torch.zeros((1)).cpu(), X=torch.zeros((1)).cpu(),
                             opts=dict(xlabel='epoch', ylabel='acc', title='training acc discriminator'))

########################################################################################################################
# Create an args class
args = Args()
args.channels_first = True
args.batch_size =1
args.val_batch_size=1
args.diff_model_flag = False
args.alpha = 1
args.learning_rate = 1e-4

LOAD_PATH_ENCODER =config.loadfile+'pretrain_encoder_line'+config.numrow
LOAD_PATH_REGRESSOR = config.loadfile+'pretrain_regressor_line'+config.numrow
LOAD_PATH_DOMAIN = config.loadfile+'pretrain_domain_line'+config.numrow

PATH_big_Rest_test=['./test_data_source2d.csv']
PATH_big_Msadv_test=['./test_data_target2d.csv']

CHK_PATH_DOMAIN = 'domain_chk_pth'
LOSS_PATH = 'loss_pth'
id=0
cuda=torch.cuda.set_device(id)

########################################################################################################################
im_size = (256,256)
#
print('numrow', config.numrow)
print('Creating datasets and dataloaders')
b_val_dataset = numpy_dataset_three(index=PATH_big_Rest_test[0], numrow=int(config.numrow))
o_val_dataset = numpy_dataset_three(index=PATH_big_Msadv_test[0], numrow=int(config.numrow))

b_val_dataloader = DataLoader(b_val_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)
o_val_dataloader = DataLoader(o_val_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)

b_int_val_dataset = numpy_dataset(index=PATH_big_Rest_test[0], numrow=int(config.numrow))
o_int_val_dataset = numpy_dataset(index=PATH_big_Msadv_test[0], numrow=int(config.numrow))

b_int_val_dataloader = DataLoader(b_int_val_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)
o_int_val_dataloader = DataLoader(o_int_val_dataset, batch_size=int(args.batch_size), shuffle=True, num_workers=0)

# Load in the model
encoder = Encoder()
regressor = Regressor()
domain_predictor = DomainPredictor(nodes=2)

if cuda:
    encoder = encoder.cuda()
    regressor = regressor.cuda()
    domain_predictor = domain_predictor.cuda()

# Make everything parallelisable
encoder = nn.DataParallel(encoder)
regressor = nn.DataParallel(regressor)
domain_predictor = nn.DataParallel(domain_predictor)

if LOAD_PATH_ENCODER:
    print('Loading Weights')
    encoder_dict = encoder.state_dict()
    pretrained_dict = torch.load(LOAD_PATH_ENCODER)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in encoder_dict}
    print('weights loaded encoder = ', len(pretrained_dict), '/', len(encoder_dict))
    encoder.load_state_dict(torch.load(LOAD_PATH_ENCODER))

if LOAD_PATH_REGRESSOR:
    regressor_dict = regressor.state_dict()
    pretrained_dict = torch.load(LOAD_PATH_REGRESSOR)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in regressor_dict}
    print('weights loaded regressor = ', len(pretrained_dict), '/', len(regressor_dict))
    regressor.load_state_dict(torch.load(LOAD_PATH_REGRESSOR))

if LOAD_PATH_DOMAIN:
    domain_dict = domain_predictor.state_dict()
    pretrained_dict = torch.load(LOAD_PATH_DOMAIN)
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in domain_dict}
    print('weights loaded domain predictor = ', len(pretrained_dict), '/', len(domain_dict))
    domain_predictor.load_state_dict(torch.load(LOAD_PATH_DOMAIN))

criteron=nn.CrossEntropyLoss()
domain_criterion = nn.BCELoss()
conf_criterion = confusion_loss()
if cuda:
    criteron = criteron.cuda()
    domain_criterion = domain_criterion.cuda()
    conf_criterion = conf_criterion.cuda()
early_stopping = EarlyStopping_unlearning(args.patience, verbose=False)
with torch.no_grad():
    epoch_reached = args.epoch_reached  # Change this back to 1
    loss_store = []

    models = [encoder.eval(), regressor.eval(), domain_predictor.eval()]
    val_dataloaders = [b_val_dataloader, o_val_dataloader, b_int_val_dataloader, o_int_val_dataloader]
    criterions = [criteron, conf_criterion, domain_criterion]
    i=0
    test_loss, test_acc_domain, test_acc_rest, test_acc_msadv, kappa_rest, kappa_msadv, auc_rest, auc_msadv = val_encoder_domain_unlearn_distinct(args, models, val_dataloaders, criterions)
    print('acc_domain', test_acc_domain, 'acc rest', test_acc_rest, 'acc_masdv', test_acc_msadv)
    print('kappa rest',  kappa_rest,'kappa_msadv', kappa_msadv,'aucrest',  auc_rest, 'aucmasdv', auc_msadv)

    val_loss, val_acc, val_acc_target_rest, val_acc_target_msadv, kappa_rest, kappa_msadv, auc_rest, auc_msadv = val_unlearn_distinct(args, models, val_dataloaders,criterions)

    print('acc_domain2', val_acc, 'acc rest2', val_acc_target_rest, 'acc_masdv2',val_acc_target_msadv)
   # Decide whether the model should stop training or not
    print('kappa rest', kappa_rest, 'kappa_msadv', kappa_msadv, 'aucrest', auc_rest, 'aucmasdv', auc_msadv)
    torch.cuda.empty_cache()  # Clear memory cache

